<?php declare(strict_types=1);

namespace App\DataMapper;

use App\Entity\EntityInterface;
use App\Factory\MonsterFactory;
use MongoDB\Model\BSONDocument;
use App\Repository\RepositoryInterface;

/**
 * Class MonsterDataMapper
 * @package App\DataMapper
 */
class MonsterDataMapper implements DataMapperInterface
{
    /**
     * @var MonsterFactory $monsterFactory
     */
    private $monsterFactory;

    /**
     * MonsterDataMapper constructor.
     * @param MonsterFactory $monsterFactory
     */
    public function __construct(MonsterFactory $monsterFactory)
    {
        $this->monsterFactory = $monsterFactory;
    }

    /**
     * @param BSONDocument $data
     * @return EntityInterface
     */
    public function setData(BSONDocument $data): EntityInterface
    {
        $mongoId = property_exists($data, RepositoryInterface::COLUMN_MONGO_ID) ? $data->_id->__toString(): '';
        return $this->monsterFactory->create()->setName($data->name)
            ->setMongoId($mongoId);
    }

    /**
     * @param EntityInterface $monster
     * @return array
     */
    public function toArray(EntityInterface $monster): array
    {
        $id = [RepositoryInterface::COLUMN_ID => $monster->getMongoId() ?? $monster->getId()];
        return array_merge($id, [
            RepositoryInterface::COLUMN_NAME => $monster->getName()
        ]);
    }
}
