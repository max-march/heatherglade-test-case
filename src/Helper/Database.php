<?php declare(strict_types=1);

namespace App\Helper;

use App\Factory\MongoFactory;
use App\Factory\PDOFactory;
use MongoDB\Database as MongoDatabase;
use PDO;

/**
 * Class Database
 * @package App\Helper
 */
class Database
{
    /**
     * @var PDO
     */
    private $sqlConnection;

    /**
     * @var MongoDatabase
     */
    private $mongoConnection;

    /**
     * Database constructor.
     * @param PDOFactory $sqlConnection
     * @param MongoFactory $mongoConnection
     */
    public function __construct(PDOFactory $sqlConnection, MongoFactory $mongoConnection)
    {
        $this->sqlConnection = $sqlConnection->create();
        $this->mongoConnection = $mongoConnection->create();
    }

    /**
     * @return PDO
     */
    public function openSqlConnection(): PDO
    {
        return $this->sqlConnection;
    }

    /**
     * @return void
     */
    public function closeSqlConnection(): void
    {
        $this->sqlConnection = null;
    }

    /**
     * @return MongoDatabase
     */
    public function openMongoConnection(): MongoDatabase
    {
        return $this->mongoConnection;
    }

    /**
     * @return void
     */
    public function closeMongoConnection(): void
    {
        $this->mongoConnection = null;
    }
}
