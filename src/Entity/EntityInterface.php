<?php declare(strict_types=1);

namespace App\Entity;

/**
 * Interface EntityInterface
 * @package App\Entity
 */
interface EntityInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $id
     * @return EntityInterface
     */
    public function setId(int $id): EntityInterface;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return EntityInterface
     */
    public function setName(string $name): EntityInterface;

    /**
     * @return string
     */
    public function getMongoId(): ?string;

    /**
     * @param string $mongoId
     * @return EntityInterface
     */
    public function setMongoId(?string $mongoId): EntityInterface;

    /**
     * @return string
     */
    public function getClassName(): string;
}
