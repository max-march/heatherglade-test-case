<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190509134718
 * @package App\Migrations
 */
final class Version20190509134718 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('players');
        $table->addColumn('id', 'integer', ['autoincrement' => true,]);
        $table->addColumn('name', 'string', ['notnull' => false,]);
        $table->setPrimaryKey(['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $schema->dropTable('players');
    }
}
