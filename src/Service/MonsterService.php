<?php declare(strict_types=1);

namespace App\Service;

use App\DataMapper\MonsterDataMapper;
use App\Repository\MonsterSqlRepository;
use MongoDB\Model\BSONDocument;

/**
 * Class MonsterService
 * @package App\Service
 */
class MonsterService extends AbstractService
{
    /**
     * @var MonsterSqlRepository $repository
     */
    private $repository;

    /**
     * PlayerService constructor.
     * @param MonsterSqlRepository $repository
     * @param MonsterDataMapper $dataMapper
     */
    public function __construct(MonsterSqlRepository $repository, MonsterDataMapper $dataMapper)
    {
        parent::__construct($dataMapper);
        $this->repository = $repository;
    }

    /**
     * @param int $playersId
     * @return array
     */
    public function getMonsters(int $playersId): array
    {
        $monsters = $this->repository->fetchMonsters($playersId);
        return $this->toArray($monsters);
    }

    /**
     * @param $rawMonsters
     * @return array
     */
    public function getMonstersFromMongo($rawMonsters): array
    {
        if ($rawMonsters instanceof BSONDocument) {
            $monsters = $this->dataMapper->setData($rawMonsters);
            return $this->dataMapper->toArray($monsters);
        }

        $data = [];
        foreach ($rawMonsters as $rawMonster) {
            $monster = $this->dataMapper->setData($rawMonster);
            $data[] = $this->dataMapper->toArray($monster);
        }

        return $data;
    }
}
