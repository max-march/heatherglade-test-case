<?php declare(strict_types=1);

namespace App\Factory;

use App\Entity\Monster;

/**
 * Class MonsterFactory
 * @package App\Factory
 */
class MonsterFactory
{
    /**
     * @return Monster
     */
    public function create(): Monster
    {
        return new Monster();
    }
}
