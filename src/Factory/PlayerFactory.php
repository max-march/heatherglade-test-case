<?php declare(strict_types=1);

namespace App\Factory;

use App\Entity\Player;

/**
 * Class PlayerFactory
 * @package App\Factory
 */
class PlayerFactory
{
    /**
     * @return Player
     */
    public function create(): Player
    {
        return new Player();
    }
}
