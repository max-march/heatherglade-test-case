<?php declare(strict_types=1);

namespace App\Factory;

use MongoDB\BSON\ObjectID;

/**
 * Class MongoIdFactory
 * @package App\Factory
 */
class MongoIdFactory
{
    /**
     * @param string $id
     * @return ObjectID
     */
    public function create(string $id): ObjectID
    {
        return new ObjectID($id);
    }
}
