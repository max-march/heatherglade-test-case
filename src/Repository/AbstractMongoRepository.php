<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\EntityInterface;
use App\Factory\MongoIdFactory;
use App\Helper\Database;
use MongoDB\Database as MongoDatabase;
use MongoDB\Model\BSONDocument;

/**
 * Class AbstractMongoRepository
 * @package App\Repository
 */
abstract class AbstractMongoRepository implements RepositoryInterface
{

    /**
     * @var Database $connection
     */
    protected $database;

    /**
     * @var string $collection
     */
    protected $collection;

    /**
     * @var MongoIdFactory $mongoIdFactory
     */
    protected $mongoIdFactory;

    /**
     * AbstractMongoRepository constructor.
     * @param Database $database
     * @param MongoIdFactory $mongoIdFactory
     * @param string $collection
     */
    protected function __construct(
        Database $database,
        MongoIdFactory $mongoIdFactory,
        string $collection
    ) {
        $this->database = $database;
        $this->mongoIdFactory = $mongoIdFactory;
        $this->collection = $collection;
    }

    /**
     * @return BSONDocument[]|null
     */
    public function fetchAll(): ?array
    {
        $collection = $this->getConnection()->selectCollection($this->collection);
        return $collection->find()->toArray();
    }

    /**
     * @param string $id
     * @return array|BSONDocument|object|null
     */
    public function fetchById(string $id)
    {
        $collection = $this->getConnection()->selectCollection($this->collection);
        return $collection->findOne([self::COLUMN_MONGO_ID => $this->mongoIdFactory->create($id)]);
    }

    /**
     * @param array $criteria
     * @return BSONDocument[]|null
     */
    public function fetchBy(array $criteria): ?array
    {
        $collection = $this->getConnection()->selectCollection($this->collection);
        return $collection->find($criteria)->toArray();
    }

    /**
     * @param int $id
     * @return EntityInterface|null
     */
    public function fetch(int $id): ?EntityInterface
    {
    }

    /**
     * @return MongoDatabase
     */
    protected function getConnection(): MongoDatabase
    {
        return $this->database->openMongoConnection();
    }

    /**
     * AbstractRepository destructor.
     */
    protected function __destruct()
    {
        $this->database->closeMongoConnection();
    }
}
