<?php declare(strict_types=1);

namespace App\Repository;

use App\Factory\MongoIdFactory;
use App\Helper\Database;
use MongoDB\Model\BSONArray;

/**
 * Class PlayersMongoRepository
 * @package App\Repository
 */
class PlayerMongoRepository extends AbstractMongoRepository
{
    public const COLUMN_NAME = 'name';

    private const COLLECTION_NAME = 'players';
    private const COLUMN_NAME_MONSTERS = 'monsters';
    private const MONGO_OPERATION_SET = '$set';

    /**
     * PlayerMongoRepository constructor.
     * @param Database $database
     * @param MongoIdFactory $mongoIdFactory
     */
    public function __construct(Database $database, MongoIdFactory $mongoIdFactory)
    {
        parent::__construct($database, $mongoIdFactory, self::COLLECTION_NAME);
    }

    /**
     * @param string $id
     * @return BSONArray
     */
    public function getMonsters(string $id)
    {
        return $this->fetchById($id)->monsters;
    }

    /**
     * @param string $playerId
     * @param array $monsters
     */
    public function addMonster(string $playerId, array $monsters): void
    {
        $collection = $this->getConnection()->selectCollection($this->collection);
        $collection->updateOne(
            [self::COLUMN_MONGO_ID => $this->mongoIdFactory->create($playerId)],
            [self::MONGO_OPERATION_SET => [self::COLUMN_NAME_MONSTERS => $monsters]]
        );
    }
}
