<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Interface ControllerInterface
 * @package App\Controller
 */
interface ControllerInterface
{
    /**
     * @param int $code
     * @return JsonResponse|null
     */
    public function sendResponse(int $code): ?JsonResponse;

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function sendResponseWithData(array $data): JsonResponse;
}
