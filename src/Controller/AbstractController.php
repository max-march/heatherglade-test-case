<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as BaseController;

/**
 * Class AbstractController
 * @package App\Controller
 */
abstract class AbstractController extends BaseController implements ControllerInterface
{
    public const NOT_FOUND_RESPONSE_ARRAY = [
        self::RESPONSE_KEY_SUCCESS => false,
        self::RESPONSE_KEY_STATUS => Response::HTTP_NOT_FOUND
    ];

    protected const RESPONSE_KEY_ERRORS = 'errors';

    protected const RESPONSE_KEY_STATUS = 'status';
    protected const RESPONSE_KEY_SUCCESS = 'success';
    protected const RESPONSE_KEY_PAYLOAD = 'payload';

    protected const CREATED_RESPONSE_ARRAY = [
        self::RESPONSE_KEY_SUCCESS => true,
        self::RESPONSE_KEY_STATUS => Response::HTTP_CREATED,
    ];

    protected const SUCCESS_RESPONSE_ARRAY = [
        self::RESPONSE_KEY_SUCCESS => true,
        self::RESPONSE_KEY_STATUS => Response::HTTP_OK,
    ];

    protected const RESPONSES = [
        Response::HTTP_CREATED => self::CREATED_RESPONSE_ARRAY,
        Response::HTTP_OK => self::SUCCESS_RESPONSE_ARRAY,
        Response::HTTP_NOT_FOUND => self::NOT_FOUND_RESPONSE_ARRAY
    ];

    /**
     * @param int $code
     * @return JsonResponse|null
     */
    public function sendResponse(int $code): ?JsonResponse
    {
        if (array_key_exists($code, self::RESPONSES)) {
            return  $this->json(self::RESPONSES[$code], $code);
        }

        return null;
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function sendResponseWithData(array $data): JsonResponse
    {
        return  $this->json(
            array_merge(self::SUCCESS_RESPONSE_ARRAY, [self::RESPONSE_KEY_PAYLOAD => $data]),
            Response::HTTP_OK
        );
    }
}
