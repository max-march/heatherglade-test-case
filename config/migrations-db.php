<?php declare(strict_types=1);

use App\Helper\EnvHelper;
use Symfony\Component\Dotenv\Dotenv;

(new Dotenv())->load(__DIR__ . '/../.env');

return [
    'dbname' => EnvHelper::getParam('DB_SQL_DATABASE'),
    'user' => EnvHelper::getParam('DB_SQL_USERNAME'),
    'password' => EnvHelper::getParam('DB_SQL_PASSWORD'),
    'host' => EnvHelper::getParam('DB_SQL_HOST'),
    'driver' => 'pdo_mysql',
];
