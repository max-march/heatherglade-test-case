Test case for Heatherglade
============================

CONFIGURATION
-------------

### Project starting

Step 1: Run composer for get all packages:

```
composer install
```

Step 2: Create .env from .env.example and configurate it

Step 3: Run command for creating and filling database

```
$ ./create-and-fill-tables
```

Step 3: Create test mongo record
```
$ mongo
> use hearherglade;
> db.players.insert( { name: "test", monsters: { name: "test" } })
```

### Information


Link on apidoc

```
http://heatherglade.loc/apidoc/
```