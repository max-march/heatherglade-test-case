define({ "api": [
  {
    "type": "get",
    "url": "/players/:id/monsters",
    "title": "Request Player monsters information",
    "name": "GetPlayerMonsters",
    "group": "Player",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Player unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": \"1\",\n  \"name\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Controller/PlayerController.php",
    "groupTitle": "Player"
  },
  {
    "type": "get",
    "url": "/players",
    "title": "Request Players information",
    "name": "GetPlayers",
    "group": "Player",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": \"1\",\n  \"name\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Controller/PlayerController.php",
    "groupTitle": "Player"
  },
  {
    "type": "post",
    "url": "/players/:id/monster",
    "title": "Request Open monster by player",
    "name": "OpenMonsterByPlayer",
    "group": "Player",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Player unique ID.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"monster\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/Controller/PlayerController.php",
    "groupTitle": "Player"
  }
] });
