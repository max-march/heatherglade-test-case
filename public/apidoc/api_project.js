define({
  "name": "Heatherglade test task",
  "version": "0.1.0",
  "description": "apiDoc basic example",
  "title": "Custom apiDoc browser title",
  "url": "http://heatherglade.loc/api/v1",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-05-11T21:26:40.876Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
